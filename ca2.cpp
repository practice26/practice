﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <malloc.h>
#include <ctime>

unsigned short int assignment = 0;
unsigned short int comparing = 0;

void bubleSort(int arr[], int size);

void qsort(int* A, int size)
{
    int i = 0;
    int j = size - 1;

    //Центральный элемент массива
    int mid = A[size / 2];


    if (size < 5)
    {
        bubleSort(A, size);
        j = 0;
        i = size;
    }
    else {
        //Делим массив
        do {
            //В левой части массива пропускаем элементы меньше центрального
            while (A[i] < mid)
            {
                comparing++;
                i++;
            }
            //В правой части пропускаем элементы больше центрального
            while (A[j] > mid) 
            {
                comparing++;
                j--;
            }

            //Меняем элементы местами
            if (i <= j) 
            {
                assignment++;
                comparing++;
                int temp = A[i];
                A[i] = A[j];
                A[j] = temp;

                i++;
                j--;
            }
        } while (i <= j);
    }

    //Рекурсивные вызовы
    if (j > 0) {
        comparing++;
        qsort(A, j + 1);
    }
    if (i < size) {
        comparing++;
        qsort(&A[i], size - i);
    }
}

void bubleSort(int arr[], int size) {
    for (int i = 0; i < size - 1; i++)
    {
        for (int j = (size - 1); j > i; j--) 
        {
            if (arr[j - 1] > arr[j]) 
            {
                comparing++;
                assignment++;
                int temp = arr[j - 1]; 
                arr[j - 1] = arr[j];
                arr[j] = temp;
            }
        }
    }
};

int main() {
    srand(time(0));
    FILE* input;
    FILE* output;

    input = fopen("input.txt", "r");
    int n;
    fscanf(input, "%d", &n);

    int* a = (int*)malloc(sizeof(int) * n);
    for (int i = 0; i < n; i++)
    {
        fscanf(input, "%d", &a[i]);
    }
    fclose(input);

    qsort(a, n);

    output = fopen("output.txt", "w");
    for (int i = 0; i < n; i++) 
    {
        fprintf(output, "%d\n", a[i]);
    }

    printf("%.3f\ncomparing\n", clock() / 1000.0);
    printf("%d\nassignment\n%d", comparing, assignment);

    fclose(output);
    free(a);
}

//Содержимое консоли
//0.174
//
//2115
//
//32378
