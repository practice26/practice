﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#define N 10000

int main()
{
    srand(938274);

    FILE* output = fopen("input.txt", "w");
    fprintf(output, "%d\n", N);
    for (int i = 0; i < N; i++)
    fprintf(output, "%d\n", rand());
    fclose(output);
}

